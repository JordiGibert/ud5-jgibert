import javax.swing.JOptionPane;

public class MainApp {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String dia=JOptionPane.showInputDialog("Introdueix el dia de la setmana");
		Boolean laboral = false;
		Boolean esDia =false;
		switch (dia) {
		case "Dilluns":
			laboral=true;
			esDia=true;
			break;
		case "Dimarts":
			laboral=true;
			esDia=true;
			break;
		case "Dimecres":
			laboral=true;
			esDia=true;
			break;
		case "Dijous":
			laboral=true;
			esDia=true;
			break;
		case "Divendres":
			laboral=true;
			esDia=true;
			break;
		case "Dissabte":
			laboral=false;
			esDia=true;
			break;
		case "Diumenge":
			laboral=false;
			esDia=true;
			break;
		default:
			esDia=false;
		}
		if (esDia) {
			if (laboral) {
				JOptionPane.showMessageDialog(null, dia+" es laboral");
			} else {
				JOptionPane.showMessageDialog(null, dia+" no es laboral");		
			}
		} else {
			JOptionPane.showMessageDialog(null, dia+" no es un dia");
		}
	}

}
