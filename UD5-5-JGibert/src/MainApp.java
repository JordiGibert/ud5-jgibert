import javax.swing.JOptionPane;
public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String numS=JOptionPane.showInputDialog("Introdueix un nombre enter");
		int numDiv= Integer.parseInt(numS);
		if(numDiv%2==0) {
			JOptionPane.showMessageDialog(null, numDiv+" es pot dividir entre 2");
		} else {
			JOptionPane.showMessageDialog(null, numDiv+" no es pot dividir entre 2");
		}
		
	}

}
