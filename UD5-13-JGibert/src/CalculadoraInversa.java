import javax.swing.JOptionPane;

public class CalculadoraInversa {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String numS=JOptionPane.showInputDialog("Introdueix un nombre");
		Double num1=Double.parseDouble(numS);
		numS=JOptionPane.showInputDialog("Introdueix un altre nombre");
		Double num2=Double.parseDouble(numS);
		String simbol=JOptionPane.showInputDialog("Introdueix un simbol aritmetic");
		Double resultat=0.0;
		switch (simbol) {
			case "+":
				resultat=num1+num2;
				break;
			case "-":
				resultat=num1-num2;
				break;
			case "*":
				resultat=num1*num2;
				break;
			case "/":
				resultat=num1/num2;
				break;
			case "%":
				resultat=num1%num2;
				break;
			case "^":
				resultat=num1;
				for(int i=1;i<num2;i++) {
					resultat=resultat*num1;
				}
				break;
		}
		JOptionPane.showMessageDialog(null, "El resultat de la operacio es "+resultat);
	}
}
