import javax.swing.JOptionPane;
public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String numS=JOptionPane.showInputDialog("Introdueix el nombre de ventes");
		int numVentas= Integer.parseInt(numS);
		Double total=0.0;
		for(int i=0;i<numVentas;i++) {
			numS=JOptionPane.showInputDialog("Introdueix el preu de la venta numero "+(i+1));
			Double producte=Double.parseDouble(numS);
			total=total+producte;
		}
		JOptionPane.showMessageDialog(null, "El total de la compra es "+total);
	}

}
